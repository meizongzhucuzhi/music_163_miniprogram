// pages/Login/Login.js
const request = require('../../services/network')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //电话号码
    phoneNumber:'',
    //密码
    password:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //处理手机号输入框
  handleNumber(e){
    this.setData({phoneNumber:e.detail.value})
  },
  //处理密码的输入框
  handlePassWord(e){
    this.setData({password:e.detail.value})
  },
  //登里请求
  handleLogin(){
    //先验证输入的手机号格式对不对
   let reg = /^1[3|5|7|8|9]\d{9}/g
   if(reg.test(this.data.phoneNumber) && this.data.password.length){
     this.login()
   }else{
     wx.showToast({
       title: '手机号不符规范或者密码没填',
       duration:3000,
       icon:'error'
     })
     this.setData({phoneNumber:''})
   }
  },
  //发送login请求
  async login(){
    const res = await request({url:'/login/cellphone',data:{phone:this.data.phoneNumber,password:this.data.password}})
    console.log(res)
    if(res.data.code === 200){
      //说明登录成功
      //保存用户信息
      wx.setStorageSync('profile', res.data.profile)
      //保存用户cookie
      wx.setStorageSync('cookie', res.data.cookie)
      wx.showToast({
        title: '登录成功',
        success(){
          wx.switchTab({
            url: '../profile/profile'
          })
        }
      })
    }else{
      //说明登录失败
      wx.showToast({
        title:'登录失败请重试',
        icon:'error',
        success:()=>{
          this.setData({
            phoneNumber:'',
            password:''
          })
        }
      })
    }
  }
  
})