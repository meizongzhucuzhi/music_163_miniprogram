// pages/singer-list/singer-list.js
const request = require("../../services/network");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //推荐歌手数据
    recommendList:[],
    //轮播图数据
    banners:[],
    //按地区分类
    areaList:[
      {
        id:7,
        txt:"华语"
      },
      {
        id:96,
        txt:"欧美"
      },
      {
        id:8,
        txt:"日本"
      },
      {
        id:16,
        txt:"韩国"
      },
      {
        id:0,
        txt:"其他"
      }
    ],
    //按类型分类
    typeList:[
      {
        id:1,
        txt:"男歌手"
      },
      {
        id:2,
        txt:"女歌手"
      },
      {
        id:3,
        txt:"乐队/组合"
      }
    ],
    //字符列表
    charList:[{id:-1,char:'热'}],
    //歌手列表数据
    singerList:[],
    //区域ID
    areaId:7,
    //类型ID
    typeId:1,
    //字符ID
    charId:-1,
    //当前页数
    page:1,
    //当前分类个数数据是否已经完全加载完毕
    hasMoreData:true,
    //返回顶部按钮是否出现
    isBackTopShow:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      for(let i = 97; i <= 122; i++){
        let id = String.fromCharCode(i);
        let char = id.toUpperCase();
        this.data.charList.push({id,char});
      }
      this.setData({
        charList:this.data.charList
      });
      this.getRecommendList();
      this.getBanner();
      this.getSingerCateList();
      
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
     console.log("到达页面底部了,开始请求下一页数据");
     this.data.page+=1;
     //如果还有下一页数据的话
     if(this.data.hasMoreData){
       this.getSingerCateList();
     }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //监听页面滚动的函数
  onPageScroll(res){
    this.setData({
      isBackTopShow: res.scrollTop > 2000
    })
  },
  //改变区域
  switchArea(e){
    this.setData({
      areaId:e.currentTarget.dataset.id,
      hasMoreData:false,
      page:1,
      singerList:[]
    });
    //更新歌手列表数据
    this.getSingerCateList(this.data.typeId,this.data.areaId,this.data.charId);
  },
  //改变类型
  switchType(e){
    this.setData({
      typeId:e.currentTarget.dataset.id,
      hasMoreData:false,
      page:1,
      singerList:[]
    });
    //更新歌手列表数据
    this.getSingerCateList(this.data.typeId,this.data.areaId,this.data.charId);
  },
  //改变字符
  switchChar(e){
    this.setData({
      charId:e.currentTarget.dataset.id,
      hasMoreData:false,
      page:1,
      singerList:[]
    });
    //更新歌手列表数据
    this.getSingerCateList(this.data.typeId,this.data.areaId,this.data.charId);
  },
  //返回顶部
  goToTop(){
    console.log('是时候要返回顶部了');
    wx.pageScrollTo({
      scrollTop:0,
      duration:666
    })
  },
  //去歌手详情页
  goSingerDetail(e){
    const id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../singer-detail/singer-detail?id='+id
    })
  },
  //获取推荐歌手列表数据
  async getRecommendList(){
    const res = await request({url:"/top/artists",data:{limit:20}});
    this.setData({
      recommendList:res.data.artists
    });
  },
  //获取banner数据
  async getBanner(){
    const res = await request({url:"/banner",type:2});
    this.setData({
      banners:res.data.banners
    });
  },
  //获取歌手分类列表数据
  async getSingerCateList(type=1,area=7,initial=-1){
     let offset = 30 * (this.data.page);
     const res = await request({url:"/artist/list",data:{type,area,initial,offset}});
     this.data.singerList.push(...res.data.artists);
     this.setData({
       singerList:this.data.singerList,
       hasMoreData:res.data.more
     });
  }
})