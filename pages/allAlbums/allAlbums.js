// pages/allAlbums/allAlbums.js
const request = require('../../services/network')
const {formatDate} = require('../../utils/util')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //歌手ID
    singerId:'',
    //当前页数
    currentPage:1,
    //每页请求的数量
    limit:10,
    //专辑列表数据
    albumList:[],
    //是否全部加载
    isAllLoad:false,
    //是否显示返回顶部按钮
    isBackTopShow:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.singerId = options.id
    this.getAlbumData(options.id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log('开始进行下拉刷新操作')
    this.data.currentPage = 1
    this.data.albumList = []
    this.getAlbumData(this.data.singerId)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('是时候请求下一页数据了')
    if(this.data.isAllLoad){
      wx.showToast({
        title: '已经全部加载',
        icon:'error',
        duration:3000
      })
    }else{
      this.data.currentPage += 1
      this.getAlbumData(this.data.singerId)
    }
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //监听页面滚动的事件
  onPageScroll(res){
    if(res.scrollTop > 1000 && !this.data.isBackTopShow){
      this.setData({
        isBackTopShow:true
      })
    }
    if(res.scrollTop <=1000 && this.data.isBackTopShow){
      this.setData({
        isBackTopShow:false
      })
    }
  },
  //返回顶部
  goToTop(){
    wx.pageScrollTo({
      scrollTop:0,
      duration: 333
    })
  },
  //去专辑详情页
  toAlbumDeatil(e){
    wx.navigateTo({
      url: '../album-detail/album-detail?id='+e.currentTarget.dataset.id
    })
  },
  //获取专辑列表数据
  async getAlbumData(singerId){
    const res = await request({url:'/artist/album?id='+singerId+'&limit='+this.data.limit+'&offset='+(this.data.currentPage-1) * this.data.limit})
    this.data.isAllLoad = !res.data.more
    res.data.hotAlbums.forEach(item=>{
      item.publishTime = formatDate(item.publishTime)
    })
    this.data.albumList.push(...res.data.hotAlbums)
    this.setData({
      albumList:this.data.albumList
    })
  }
})