// pages/video/video.js
const request = require('../../services/network')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //视频分类
    cateList:[],
    //当前视频分类index
    currentIndex:0,
    //视频列表数据
    videoList:[],
    //视频列表高度
    videoLishH:0,
    //当前正在播放视频的id
    currentVid:'',
    //播放视频的记录
    videoRecord:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const profile = wx.getStorageSync('profile') || {}
    if(profile.nickname){
      this.getVideoCates()
      const systemInfo = wx.getSystemInfoSync()
      this.setData({videoLishH: (systemInfo.windowHeight*2 - 86) })
    }else{
      wx.reLaunch({
        url: '../login/login'
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      path:"pages/video/video",
      title:"视频"
    }
  },
  //切换视频分类
  switchCate(e){
   let {index,id} = e.currentTarget.dataset
   this.setData({currentIndex:index,videoList:[]})
   //切换数据
   this.getVideoListByCate(id)
  },
  //播放视频
  playVideo(e){
    let {vid,index} = e.currentTarget.dataset
    //清空所有视频的videoUrl
    this.data.videoList.forEach((item)=>{
      if(this.data.currentVid !== vid){
        item.videoUrl = ''
      }
    })
    //保存最新的vid
    this.data.currentVid = vid
    this.videoCtx = wx.createVideoContext(this.data.currentVid)
    //请求视频播放地址
    this.getVideoUrl(this.data.currentVid,index)
  },
  //视频正在播放
  timeUpdate(e){
    
  },
  //获取视频分类列表数据
  async getVideoCates(){
    const res = await request({url:'/video/group/list'})
    if(res.data.code === 200){
      this.setData({cateList:res.data.data.slice(0,16)})
      this.getVideoListByCate(this.data.cateList[0].id)
    }
  },
  //获取对应分类下的视频
  async getVideoListByCate(cateId){
    const res = await request({url:'/video/group?id='+cateId})
    if(res.data.datas.length){
      res.data.datas.forEach(item=>{
        item.videoUrl = ''
      })
      this.setData({videoList:res.data.datas})
    }
  },
  //请求视频播放地址
  async getVideoUrl(vid,index){
    const res =await request({url:'/video/url?id='+vid})
    if(res.data.urls && res.data.urls[0] && res.data.urls[0].url){
     this.data.videoList[index].videoUrl = res.data.urls[0].url
     this.setData({
       videoList:this.data.videoList
     })
    }
  }
})