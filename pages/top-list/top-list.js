// pages/top-list/top-list.js
const request = require("../../services/network");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    topList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
      this.getTopListData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //去歌单详情页
  toDetail(e){
    const {id} = e.currentTarget.dataset;
    wx.navigateTo({
      url: '../gedan-detail/gedan-detail?id='+id,
    })
  },
  async getTopListData(){
    const res = await request({url:"/toplist"});
    this.setData({topList:res.data.list})
  }
})