// pages/album-detail/album-detail.js
const request = require('../../services/network')
const {formatDate} = require('../../utils/util')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //专辑ID
    albumId:'',
    //专辑详情数据
    albumDetail:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.albumId = options.id
    this.getAlbumDeatil(options.id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title:this.data.albumDetail.info.name
    }
  },
  //播放歌曲
  playSong(e){
    wx.navigateTo({
      url: '../song/song?id='+e.currentTarget.dataset.id
    })
  },
  //获取专辑详情数据
  async getAlbumDeatil(albumId){
    const res = await request({url:'/album?id='+albumId})
    res.data.album.publishTime = formatDate(res.data.album.publishTime)
    res.data.songs.forEach((item,index)=>{
      item.index = index + 1
    })
    this.data.albumDetail.info = res.data.album
    this.data.albumDetail.songs= res.data.songs
    this.setData({
      albumDetail:this.data.albumDetail
    })
  }
})