// pages/index/index.js
const request = require("../../services/network");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //窗口可视高度
    clientHeight:0,
    //轮播图数据
    bannerData:[],
    currentNav:'0',
    currentSwiper:0,
    //推荐歌单数据
    recommendData:[],
    //新碟上架数据
    albumData:[],
    //排行榜榜单标题
    topListTitle:[],
    //排行榜的歌曲详情
    topListSong:[],
    //第几个榜单
    toplistIndex:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad:  function (options) {

    //获取可视高度
    wx.getSystemInfo({ 
      success:(res)=>{ 
          this.setData({ 
              clientHeight: res.windowHeight 
          }); 
      } 
    });
    //首页相关数据请求
    this.getBannerData();
    this.getRecommendData();
    this.getAlbumData();
    this.getTopListTitleData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //切换首页分类
  switchNav(e){
    this.setData({
      currentNav:e.currentTarget.dataset.index,
      currentSwiper:+e.currentTarget.dataset.index
    });
  },
  //切换swiper
  changeSwiper(e){
    this.setData({
      currentNav:e.detail.current + "",
      currentSwiper:e.detail.current
    });
  },
  //切换排行榜的swiper
  changetoplist(e){
      this.setData({
        toplistIndex:e.detail.current
      });
      //要重新拉取歌曲数据
      this.getTopListSongsData();
  },
  //去歌单列表页
  toPlayList(){
    wx.navigateTo({
      url: '../play-list/play-list',
    })
  },
  //去排行榜列表页
  toTopList(){
    wx.navigateTo({
      url: '../top-list/top-list',
    })
  },
  //去歌单列表页
  toSingerList(){
    wx.navigateTo({
      url: '../singer-list/singer-list',
    })
  },
  //去歌单详情页
  toGedanDetail(e){
    wx.navigateTo({
      url: '../gedan-detail/gedan-detail?id='+e.currentTarget.dataset.id
    })
  },
   //去专辑详情页
   toAlbumDeatil(e){
    wx.navigateTo({
      url: '../album-detail/album-detail?id='+e.currentTarget.dataset.id
    })
  },
  //去歌曲详情页播放歌曲
  playSong(e){
    wx.navigateTo({
      url: '../song/song?id='+e.currentTarget.dataset.id
    })
  },
  //去每日推荐页面
  toRecommend(){
    wx.navigateTo({
      url: '../day-recommend/day-recommend',
    })
  },
  //请求轮播图数据
  async getBannerData(){
    const res = await request({url:"/banner",data:{type:2}});
    this.setData({
      bannerData:res.data.banners
    });
  },
  //获取首页推荐歌单数据
  async getRecommendData(){
    const res = await request({url:"/personalized",data:{limit:12}});
    this.setData({
      recommendData:res.data.result
    });
  },
  //获取首页新碟上架数据
  async getAlbumData(){
    const res = await request({url:"/album/newest"});
    this.setData({
      albumData:res.data.albums
    });
  },
  //获取首页排行榜的标题数据
  async getTopListTitleData(){
    const res = await request({url:"/toplist"});
    const arr = res.data.list.slice(0,6);
    this.setData({
      topListTitle:arr
    });
   this.getTopListSongsData();
  },
  //获取排行榜歌曲列表的数据
  async getTopListSongsData(){
    const results = await request({url:"/playlist/detail",data:{id:this.data.topListTitle[this.data.toplistIndex].id}});
    const trackIds = results.data.playlist.trackIds.slice(0,3);
    //拿排行榜下面的歌单列表数据
    let ids = "";
    const idArr =[];
    trackIds.forEach(item=>{
      idArr.push(item.id);
    });
    ids = idArr.join(",");
    const songList = await request({url:"/song/detail",data:{ids}});
    this.setData({
      topListSong:songList.data.songs
    });
  }
})