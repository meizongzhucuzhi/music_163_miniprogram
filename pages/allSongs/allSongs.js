// pages/allSongs/allSongs.js
const request = require('../../services/network')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //歌手ID
    singerId:'',
    //歌手的所有歌曲数据
    allSongs:[],
    //每页请求歌曲数量
    limit:20,
    //当前页数
    currentPage:1,
    //返回顶部按钮是否显示
    isBackTopShow:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.name){
      wx.setNavigationBarTitle({
        title: options.name + '的所有歌曲',
      })
    }
    this.data.singerId = options.id
    this.getAllSongs(this.data.singerId)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log('下拉刷新数据')
    wx.showToast({
      title: '正在刷新数据,请稍后',
    })
    this.data.allSongs = []
    this.data.currentPage = 1
    this.getAllSongs(this.data.singerId)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
     console.log('页面滑到底部了,开始拉取下一页的数据')
     this.data.currentPage += 1
     this.getAllSongs(this.data.singerId)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //监听页面的滚动
  onPageScroll(res){
    if(res.scrollTop > 2000 && !this.data.isBackTopShow){
      this.setData({
        isBackTopShow:true
      })
    }
    if(res.scrollTop <= 2000 && this.data.isBackTopShow){
      this.setData({
        isBackTopShow:false
      })
    }
  },
  //返回顶部
  goToTop(){
    wx.pageScrollTo({
      scrollTop:0,
      duration: 300,
    })
  },
  //获取歌手的所有单曲
  async getAllSongs(singerId){
    const res = await request({url:'/artist/songs?id=' + singerId + '&limit=' + this.data.limit + '&offset=' + (this.data.currentPage-1) * this.data.limit})
    if(res.data.songs.length){
      this.data.allSongs.push(...res.data.songs)
    }else{
      wx.showToast('该歌手的全部歌曲已经加载完毕')
    }
    this.data.allSongs.forEach((item,index)=>{
      item.index = index + 1
    })
    this.setData({
      allSongs:this.data.allSongs
    })
  }
})