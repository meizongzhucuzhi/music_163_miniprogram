// pages/gedan-detail/gedan-detail.js
const request = require("../../services/network");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //歌单详情数据
    detailData:{},
    //歌单歌曲列表数据
    songList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      //请求相关数据
      this.getDetailData(options.id);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
      return {
        title:"歌单详情",
        path:"pages/gedan-detail/gedan-detail"
      }
  },
  //点击去歌曲详情页播放歌曲
  playSong(e){
    wx.navigateTo({
      url: '../song/song?id='+e.detail.id
    })
  },
  //获取歌单详情数据
  async getDetailData(id){
      const res = await request({url:"/playlist/detail",data:{id}});
      let arr = [];
      res.data.playlist.trackIds.forEach(item=>{
        arr.push(item.id);
      });
      let ids = arr.join(",");
      let result = await request({url:"/song/detail",data:{ids}});
      this.setData({
        detailData:res.data.playlist,
        songList:result.data.songs
      });
  }
})