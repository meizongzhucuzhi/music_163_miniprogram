// pages/day-recommend/day-recommend.js
const request = require('../../services/network')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentDate:{},
    songList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getCurrentDate()
    this.getDayRecommend()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //得到当前日期
  getCurrentDate(){
    let d = new Date()
    let month = (d.getMonth() + 1) < 10 ? '0' +  (d.getMonth() + 1) : '' + (d.getMonth() + 1)
    let day = d.getDate() < 10 ? '0' + d.getDate() :'' + d.getDate()
    this.data.currentDate.month = month
    this.data.currentDate.day = day
    this.setData({
      currentDate:this.data.currentDate
    })
  },
  //去歌曲详情页
  toSongDetail(e){
    console.log('歌曲详情')
    wx.navigateTo({
      url: '../song/song?id=' + e.currentTarget.dataset.id
    })
  },
  //去歌手详情页
  toSinger(e){
    console.log('歌手详情')
    wx.navigateTo({
      url: '../singer-detail/singer-detail?id='+e.currentTarget.dataset.id
    })
  },
  //播放全部歌曲
  playAll(){
    console.log('播放全部歌曲')
  },
  //获取每日歌单数据
  async getDayRecommend(){
    const res = await request({url:'/recommend/songs'})
    this.setData({songList:res.data.data.dailySongs})
  }
})