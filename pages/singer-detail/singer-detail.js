// pages/singer-detail/singer-detail.js
const request = require('../../services/network')
const {formatDate} = require('../../utils/util')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //歌手id
    singerId:'',
    isCoverShow:false,
    //歌手详情数据
    singerDetail:{},
    //歌手热门歌曲
    hotSongs:[],
    //歌曲总数:
    musicCount:0,
    //专辑数据
    albumList:[],
    //视频总数
    videoCount:0,
    //视频数据
    videoList:[],
    //相似歌手
    simiList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.singerId = options.id
    this.getSingerData(options.id)
    this.getHotSongs(options.id)
    this.getAlbumData(options.id)
    this.getVideoData(options.id)
    this.getSimiSingerData(options.id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //预览封面图片
  previewCover(){
    this.setData({
      isCoverShow:true
    })
  },
  //关闭预览封面图片
  closeCover(){
    this.setData({
      isCoverShow:false
    })
  },
  //查看歌手的所有歌曲
  seeAllSongs(){
    wx.navigateTo({
      url: '../allSongs/allSongs?id=' + this.data.singerId + '&name=' + this.data.singerDetail.artist.name
    })
  },
  //查看歌手的所有专辑
 seeAllAlbums(){
  wx.navigateTo({
    url: '../allAlbums/allAlbums?id=' + this.data.singerId
  })
 },
  //去mv页
  toMv(e){
    wx.navigateTo({
      url: '../mv/mv?id='+e.currentTarget.dataset.mid+"&name="+e.currentTarget.dataset.name+"&artistName="+e.currentTarget.dataset.artist
    })
  },
  //去歌手详情页
  toSingerDetail(e){
    wx.redirectTo({
      url: '../singer-detail/singer-detail?id='+e.currentTarget.dataset.id,
    })
  },
  //去专辑详情页
  toAlbumDeatil(e){
    wx.navigateTo({
      url: '../album-detail/album-detail?id='+e.currentTarget.dataset.id
    })
  },
  //去歌曲详情页播放歌曲
  playSong(e){
    wx.navigateTo({
      url: '../song/song?id='+e.detail.id,
    })
  },
  //获取歌手详情数据
  async getSingerData(singerId){
    const res = await request({url:'/artist/detail?id='+singerId})
    this.setData({
      singerDetail:res.data.data,
      musicCount:res.data.data.artist.musicSize,
      videoCount:res.data.data.videoCount
    })
  },
  //获取歌手热门歌曲(50首)数据
  async getHotSongs(singerId){
    const res = await request({url:'/artist/top/song?id='+singerId})
    res.data.songs.forEach((item,index)=>{
      item.index = index + 1
    })
    this.setData({
      hotSongs:res.data.songs
    })
  },
  //获取歌手专辑数据
  async getAlbumData(singerId){
    const res = await request({url:'/artist/album?id='+singerId+'&limit=15'})
    //处理时间格式
    res.data.hotAlbums.forEach(item=>{
      item.publishTime = formatDate(item.publishTime)
    })
    this.setData({
      albumList:res.data.hotAlbums
    })
  },
  //获取歌手视频数据
  async getVideoData(singerId){
    const res = await request({url:'/artist/mv?id='+singerId})
    this.setData({
      videoList:res.data.mvs,
      videoCount:res.data.mvs.length
    })
  },
  //获取相似歌手数据
  async getSimiSingerData(singerId){
    const res = await request({url:'/simi/artist?id='+singerId})
    this.setData({
      simiList:res.data.artists
    })
  }
})