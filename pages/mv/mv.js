// pages/mv/mv.js
const request = require('../../services/network')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mv:{},
    //mv的播放地址
    mvUrl:'',
    //弹幕列表
    danmuList:[],
    //相似mv
    simiMv:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.setData({mv:options})
      this.resolveDanMu()
      this.getMvUrl(this.data.mv.id)
      this.getSimiMvData(this.data.mv.id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title:this.data.mv.name
    }
  },
  //去MV详情页
  toMvDetail(e){
    const {id,name,artistname} = e.currentTarget.dataset
    wx.redirectTo({
      url: '../mv/mv?id='+id+'&name='+name+'&artistName='+artistname
    })
  },
  //去歌手详情页
  toSingerDetail(e){
    wx.navigateTo({
      url: '../singer-detail/singer-detail?id='+e.currentTarget.dataset.id,
    })
  },
  //模拟弹幕列表数据
  resolveDanMu(){
    //模拟每隔1s生成1条弹幕,总共生成100条弹幕
    for(let i=0;i<200;i++){
      this.data.danmuList.push({
        text:"都说缘许三生,但愿来世我们都可以生在平常人家,此生一诺,来世必践!",
        color:this.getRandomColor(),
        time:i
      })
    }
    this.setData({
      danmuList:this.data.danmuList
    })
  },
  //获取随机颜色
  getRandomColor(){
    let r = Math.floor(Math.random()*255)
    let g = Math.floor(Math.random()*255)
    let b = Math.floor(Math.random()*255)
    return `rgb(${r},${g},${b})`
  },
  //获取mv播放的url
  async getMvUrl(id){
    const res = await request({url:'/mv/url?id='+id})
    this.setData({
      mvUrl:res.data.data.url
    })
  },
  //获取相似mv的列表数据
  async getSimiMvData(mid){
    const res = await request({url:'/simi/mv?mvid='+mid})
    this.setData({simiMv:res.data.mvs})
  }
})