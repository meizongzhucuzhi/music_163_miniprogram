// pages/play-list/play-list.js
const request = require("../../services/network");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //可视区域高度
    clientHeight:0,
    //当前分类索引
    currentCateIndex:0,
    //分类列表
    categoryList:[],
    //歌单列表数据
    playList:[],
    //当前页数
    currentPage:0,
    //当前分类名称
    currentName:"全部歌单",
    //scroll-view是否处于下拉刷新的操作
    isRefresh:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //获取屏幕可视区域高度
    wx.getSystemInfo({
      success: (res) => {
       this.setData({
         clientHeight:res.windowHeight
       });
      },
    })
    //请求该页面相关数据
    this.getCategoryList();
    this.getPlayList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log("用户滑到了底部");
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //改变歌单分类
  switchCate(e){
    const {index,name} = e.currentTarget.dataset;
    this.setData({
      currentCateIndex:index,
      currentName:name
    });
    //切换下方的歌单数据
    //切换歌单内容后,应该要重置一些数据
    this.data.playList = [];
    this.data.currentPage = 1;
    //请求歌单列表数据
    this.getPlayList(this.data.currentName,this.data.currentPage);
  },
  //检测用户滚到了底部
  scrolltolower(){
    console.log("可以请求下一页数据了");
    this.data.currentPage+=1;
    this.getPlayList(this.data.currentName,this.data.currentPage);
  },
  //触发了下拉刷新的动作
  startRefresh(){
    console.log("可以进行下拉刷新操作了");
    this.setData({isRefresh:true});
    this.data.playList = [];
    this.data.currentPage = 1;
    this.getPlayList(this.data.currentName,this.data.currentPage);
    this.setData({isRefresh:false});
  },
  //结束下拉刷新操作
  endRefresh(e){
    console.log(e);
  },
  //滑动swiper
  switchCateContent(e){
    const current = e.detail.current;
    const name = this.data.categoryList[current].name;
    this.setData({
      currentCateIndex:current,
      currentName:name
    });
    //切换下方的歌单数据
    //切换歌单内容后,应该要重置一些数据
    this.data.playList = [];
    this.data.currentPage = 1;
    //请求歌单列表数据
    setTimeout(()=>{
      this.getPlayList(this.data.currentName,this.data.currentPage);
    },500);
  },
  //点击去歌单详情页面
  toDetail(e){
    const {id} = e.currentTarget.dataset;
    wx.navigateTo({
      url: '../gedan-detail/gedan-detail?id='+id,
    });
  },
  //获取歌单分类数据
  async getCategoryList(){
    const res = await request({url:"/playlist/catlist"});
    this.data.categoryList = res.data.sub.slice(0,10);
    this.data.categoryList.unshift(res.data.all);
    this.setData({
      categoryList:this.data.categoryList
    });
  },
  //获取对应分类下面的歌单列表数据
   async getPlayList(cat="全部歌单",page=1,limit=15){
    const res = await request({url:"/top/playlist",data:{cat,limit,offset:(page-1)*limit}});
    this.data.playList.push(...res.data.playlists);
    this.setData({
      playList:this.data.playList
    });
  }
})