// pages/profile/profile.js
const request = require('../../services/network')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //个人信息
    profile:{},
    //用户歌单
    playlist:[],
    //最近播放
    rencentList:[],
    //推荐歌单
    recommendData:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //获取缓存数据
    const profile = wx.getStorageSync('profile') || {}
    this.setData({profile})
    if(profile.userId){
      this.getUserLevel()
      this.getUserPlayList()
      this.getRecommendData()
      this.getUserRecord()
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //处理登录/退出登录的操作
  handleBtn(){
    if(this.data.profile.nickname){
      //说明是进行退出登录的操作
      wx.showModal({
        title: '退出登录',
        content: '您确定要退出登录吗?',
        confirmColor:'#f00',
        success:(res)=> {
          if (res.confirm) {
           //清除用户信息和cookie
           wx.removeStorageSync('profile')
           wx.removeStorageSync('cookie')
           wx.reLaunch({
             url: '../profile/profile'
           })
          }
        }
      })
    }else{
      //说明是要登录操作,跳到登录页就OK
      wx.reLaunch({
        url: '../login/login'
      })
    }
  },
  //去歌单详情页面
  toGedanDetail(e){
    wx.navigateTo({
      url: '../gedan-detail/gedan-detail?id='+e.currentTarget.dataset.id
    })
  },
  //去歌手详情页
  toSinger(e){
    wx.navigateTo({
      url: '../singer-detail/singer-detail?id='+e.currentTarget.dataset.id
    })
  },
  //去歌曲详情页
  toSong(e){
    wx.navigateTo({
      url: '../song/song?id='+e.currentTarget.dataset.id
    })
  },
  //获取用户歌单
  async getUserPlayList(){
    const res = await request({url:'/user/playlist?uid='+this.data.profile.userId})
    if(res.data.code === 200){
      this.setData({
        playlist:res.data.playlist
      })
    }
  },
  //获取用户等级信息
  async getUserLevel(){
    const res = await request({url:'/user/level'})
    if(res.data.code === 200){
      this.data.profile.level = res.data.data.level
      this.setData({
        profile: this.data.profile
      })
    }
  },
  //获取推荐歌单数据
  async getRecommendData(){
    const res = await request({url:'/personalized'})
    if(res.data.code === 200){
      this.setData({
        recommendData: res.data.result
      })
    } 
  },
  //获取用户最近播放记录
  async getUserRecord(){
    const res = await request({url:'/user/record?uid='+this.data.profile.userId+'&type=1'})
    if(res.data.code === 200){
      res.data.weekData.forEach(item=>{
        this.data.rencentList.push(item.song)
      })
      this.setData({rencentList:this.data.rencentList})
    }
  }
})