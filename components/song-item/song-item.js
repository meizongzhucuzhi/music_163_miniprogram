// components/song-item/song-item.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
     //歌曲对象信息
     songInfo:{
       type:Object,
       value:{}
     }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    playSong(){
      this.triggerEvent('playSong',{id:this.properties.songInfo.id})
    }
  }
})
