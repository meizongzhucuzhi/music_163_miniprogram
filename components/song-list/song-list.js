// components/song-list/song-list.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
      detailData:{
        type:Object,
        value:{}
      },
      songList:{
        type:Array,
        value:[]
      }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    //点击去歌曲详情页播放歌曲
    playSong(e){
      this.triggerEvent('playSong',{id:e.currentTarget.dataset.id})
    }
  }
})
