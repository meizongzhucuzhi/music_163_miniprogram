// components/back-top/back-top.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isBackTopShow:{
      type:Boolean,
      value:false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    handleClick(){
      this.triggerEvent('goToTop')
    }
  }
})
