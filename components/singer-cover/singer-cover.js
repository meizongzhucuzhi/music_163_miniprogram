// components/singer-cover/singer-cover.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    coverImage:{
      type:String,
      value:'/assets/images/test.jpg'
    },
    name:{
      type:String,
      value:''
    },
    introduce:{
      type:String,
      value:''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    //关闭封面图片
    closeCover(){
      this.triggerEvent('closeCover')
    },
    //保存图片
    savePic(){
      wx.getImageInfo({
        src: this.properties.coverImage,
        success:(res)=>{
         wx.getSetting({
           withSubscriptions: true,
           success:(result)=>{
            if(result.authSetting['scope.writePhotosAlbum']){
              //保存到系统相册
              wx.saveImageToPhotosAlbum({
                filePath:res.path,
                success:()=>{
                  wx.showToast({
                    title: '保存图片到相册成功',
                  })
                },
                fail:()=>{
                  wx.showToast({
                    title: '保存图片到相册失败',
                  })
                }
              })
            }else{
              wx.authorize({
                scope: 'scope.writePhotosAlbum',
                success:()=>{
                  this.savePic()
                },
                fail:()=>{
                  wx.showToast({
                    title:'你还没有授权图片保存到相册'
                  })
                }
              })
            }
           }
         })
        },
        fail:(err)=>{
          console.log(err)
        }
      })
    }
  }
})
