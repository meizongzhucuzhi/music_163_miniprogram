// components/list-item/list-item.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    //背景封面图
    coverUrl:{
      type:String,
      value:""
    },
    //观看量
    count:{
      type:String,
      value:""
    },
    //简介
    des:{
      type:String,
      value:""
    },
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
