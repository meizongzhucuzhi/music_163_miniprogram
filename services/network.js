//封装一个基于promise的网络请求
const {BASEURL} = require("../utils/config");
function request(params){
  wx.showLoading({
    title:"正在请求数据中...",
    mask:true
  });
  return new Promise((resolve,reject)=>{
    wx.request({
      url:BASEURL + params.url,
      data:params.data || {},
      dataType: "json",
      enableCache: true,
      enableHttp2: true,
      enableQuic: true,
      header:params.header || {"content-type":"application/json","cookie":wx.getStorageSync('cookie') || ''},
      method:params.method || "GET",
      timeout: 6666,
      success: function(res){
          resolve(res);
          wx.hideLoading();
      },
      fail: reject,
    })
  });
}
module.exports = request;