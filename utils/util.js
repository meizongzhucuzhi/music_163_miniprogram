const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

const formatCount = n => {
  if(n < 10000){
    return n;
  }else{
    return Math.floor(n / 10000) + "万";
  }
}
const formatDate = (n) => {
  if(typeof n !== "number"){
    throw Error("所传参数必须是数字");
  }
  let d= new Date(n)
  let year = d.getFullYear()
  let month = (d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1):(d.getMonth() + 1)
  let date = d.getDate() < 10 ? '0' + d.getDate() : d.getDate()
  return year + '-' + month + '-' + date
}
module.exports = {
  formatTime,
  formatCount,
  formatDate
}
